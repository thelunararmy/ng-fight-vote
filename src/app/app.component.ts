import { Component } from '@angular/core';
import keycloak from './keycloak';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'ng-fight-vote';

  public keycloakLogin(): void {
    keycloak.login();
  }

  public keycloakLogout(): void {
    keycloak.logout();
  }

  get loggedin(): boolean | undefined {
    return keycloak.authenticated;
  }

  get username(): string | undefined {
    return keycloak.tokenParsed?.name;
  }
}
