import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FightStaticComponent } from './components/fight-static/fight-static.component';
import { VotesComponent } from './components/votes/votes.component';
import { CanActivateService } from './services/can-activate.service';
import { TokenPageComponent } from './components/token-page/token-page.component';
import { KeycloakAuthGuardService } from './services/keycloak-auth-guard.service';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/stats' },
  { path: 'stats', component: FightStaticComponent },
  {
    path: 'votes',
    component: VotesComponent,
    canActivate: [CanActivateService],
  },
  {
    path: 'token',
    component: TokenPageComponent,
    canActivate: [KeycloakAuthGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
