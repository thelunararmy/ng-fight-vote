import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { RawUserData } from '../models/raw-user-data.model';
import { UserVote } from '../models/user-vote.model';

@Injectable({
  providedIn: 'root',
})
export class FetchUserVotesService {
  constructor(private readonly http: HttpClient) {
    this.getRawStartingVotes().subscribe(
      (votes: UserVote[]) => {
        this._userVotes = [...votes];
      },
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );
  }

  private _userVotes: UserVote[] = [];

  get userVotes(): UserVote[] {
    return this._userVotes;
  }

  get muskVoteCount(): number {
    return this._userVotes.filter((item) => item.vote === 'musk').length;
  }

  get zuckVoteCount(): number {
    return this._userVotes.filter((item) => item.vote === 'zuck').length;
  }

  getRawStartingVotes(): Observable<UserVote[]> {
    return this.http
      .get<RawUserData>(
        `https://randomuser.me/api/?results=200&password=lower,1-1`
      )
      .pipe(
        map((raw) => {
          return raw.results.map((item) => ({
            username: item.name.first + ' ' + item.name.last,
            vote: 'bdfhjlortvxz'.includes(item.login.password)
              ? 'musk'
              : 'zuck',
            image: item.picture.thumbnail,
          }));
        })
      );
  }

  public addUserVoteFromForm(username: string, vote: string): void {
    this._userVotes.push({
      username,
      vote,
      image: `https://randomuser.me/api/portraits/thumb/women/87.jpg`,
    } as UserVote);
  }
}
