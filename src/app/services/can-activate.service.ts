import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class CanActivateService {
  constructor(private readonly router: Router) {}

  private _myConditionalCheck(): boolean {
    // return false if the user is not allowed to view page
    return false;
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (this._myConditionalCheck()) {
      console.log('This page is protected');
      return false;
    } else {
      return true;
    }
  }
}
