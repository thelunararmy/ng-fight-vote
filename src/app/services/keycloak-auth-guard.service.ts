import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import keycloak from '../keycloak';

@Injectable({
  providedIn: 'root',
})
export class KeycloakAuthGuardService implements CanActivate {
  constructor(private readonly router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    if (keycloak.authenticated) {
      return true;
    } else {
      this.router.navigateByUrl('/');
      return false;
    }
  }
}
