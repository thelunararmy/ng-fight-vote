export interface UserVote {
  username: string;
  vote: string;
  image: string;
}
