import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VotesComponent } from './components/votes/votes.component';
import { FightStaticComponent } from './components/fight-static/fight-static.component';
import { VoteFormComponent } from './components/vote-form/vote-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TokenPageComponent } from './components/token-page/token-page.component';

@NgModule({
  declarations: [
    AppComponent,
    VotesComponent,
    FightStaticComponent,
    VoteFormComponent,
    TokenPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
