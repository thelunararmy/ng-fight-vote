import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-vote-form',
  templateUrl: './vote-form.component.html',
  styleUrls: ['./vote-form.component.css'],
})
export class VoteFormComponent implements OnInit {
  constructor() {}

  @Input() username: string = '';
  @Output() voteEvent: EventEmitter<string> = new EventEmitter<string>();

  public onSubmit(form: NgForm) {
    // console.log('form', form.value);
    this.voteEvent.emit(form.value.vote);
  }

  public selectedVote: string = '';
  public setSelectedVote(event: Event): void {
    this.selectedVote = (event.target as HTMLInputElement).value;
  }

  ngOnInit(): void {}
}
