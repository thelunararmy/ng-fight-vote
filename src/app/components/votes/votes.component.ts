import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { RawUserData } from 'src/app/models/raw-user-data.model';
import { UserVote } from 'src/app/models/user-vote.model';
import { FetchUserVotesService } from 'src/app/services/fetch-user-votes.service';

@Component({
  selector: 'app-votes',
  templateUrl: './votes.component.html',
  styleUrls: ['./votes.component.css'],
})
export class VotesComponent implements OnInit {
  constructor(private readonly fetchUserVotes: FetchUserVotesService) {}

  ngOnInit(): void {}

  // New code
  get userVotes(): UserVote[] {
    return this.fetchUserVotes.userVotes;
  }

  // Utility
  public emojify(vote: string): string {
    return vote === 'musk' ? '🦨' : '🤖';
  }
}
