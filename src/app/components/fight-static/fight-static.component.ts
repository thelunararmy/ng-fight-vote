import { Component, OnInit } from '@angular/core';
import keycloak from 'src/app/keycloak';
import { FetchUserVotesService } from 'src/app/services/fetch-user-votes.service';

@Component({
  selector: 'app-fight-static',
  templateUrl: './fight-static.component.html',
  styleUrls: ['./fight-static.component.css'],
})
export class FightStaticComponent implements OnInit {
  constructor(private readonly fetchUserVotes: FetchUserVotesService) {}

  ngOnInit(): void {
    this._username = keycloak.authenticated
      ? keycloak.tokenParsed?.name || ''
      : this._username;
  }

  get muskCount(): number {
    return this.fetchUserVotes.muskVoteCount;
  }

  get zuckCount(): number {
    return this.fetchUserVotes.zuckVoteCount;
  }

  // Data handling to child
  private _username: string = '';

  get username(): string {
    return this._username;
  }

  public setUsername(event: Event): void {
    this._username = (event.target as HTMLInputElement).value;
    this._username = this._username
      .split(' ')
      .map(
        (word) =>
          word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase()
      )
      .join(' ');
  }

  public handleVoteReceived(item: string): void {
    console.log('Recieved data from child:', item);
    this.fetchUserVotes.addUserVoteFromForm(this._username, item);
  }
}
