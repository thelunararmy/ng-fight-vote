import { Component, OnInit } from '@angular/core';
import keycloak from 'src/app/keycloak';

@Component({
  selector: 'app-token-page',
  templateUrl: './token-page.component.html',
  styleUrls: ['./token-page.component.css'],
})
export class TokenPageComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  get token(): string | undefined {
    return keycloak.token;
  }
}
